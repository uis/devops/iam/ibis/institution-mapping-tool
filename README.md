# Institution Mapping Tool

Tool to query Jackdaw for CHRIS and CAMSIS institutions and their mapping to
Lookup institutions.

## Usage

The sync tool needs to connect to Oracle databases and, potentially, Google
Cloud storage. If running locally you would need to install the proprietary oracle
client libraries and gcloud SDK. Fortunately, the docker container provided
contains both of these.

Also, in order to pass the google credentials into the container, a logan
configuration is provided.

So the logan tool can be called with the following to have the gcloud already
authenticated:

```bash
$ logan -- help
```

You'll need a configuration file for the sync tool so need to pass extra docker
arguments for logan to include:
```bash
$ logan --docker-run-args "-v $(pwd)/configuration.yaml:/config.yaml" \
        -- -c /config.yaml
```

The logan tool is available at:
https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan

## Configuration

Configuration can be passed in a YAML-formatted configuration file
([example](configuration.yaml.example)) via the `--configuration` option or via
environment variable.

Environment variables start `INSTSYNC_` and then follow the naming of settings
in the configuration file. Nested fields can be specified using a `__`
delimiter. For example, to override the Jackdaw username from that supplied in
`configuration.yaml`:

```console
$ INSTSYNC_JACKDAW__USERNAME=myuser instsync --configuration configuration.yaml
```

Note that configuration in environment variables will override any configuration
passed via `--configuration`.

## CI Configuration

The following CI variables need to be available for a sync job to run:

* `CONFIGURATION`: A "file-like" CI-variable which expands to a path to a file
    containing the configuration. Currently this is configure to upload to a
    Google Cloud Storage bucket.
* `GOOGLE_APPLICATION_CREDENTIALS`: A "file-like" CI-variable which expands to a
    path to Google service account credentials which should be used to upload to
    a bucket.
* `PERFORM_SYNC`: As long as this variable has some value, the job will run.

Both of these variables are marked are "protected-only" meaning they only run on
protected branches and not on merge request branches.
