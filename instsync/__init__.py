"""
Card Database synchronisation tool

Usage:
    instsync (-h | --help)
    instsync [--quiet] [--debug] [--configuration=FILE]...

Options:
    -h, --help                  Show a brief usage summary.

    --quiet                     Reduce logging verbosity.
    --debug                     Log debugging information

    -c, --configuration=FILE    Specify configuration file to load which has
                                credentials to access database.
"""
import json
import logging
import os
import sys

import docopt

from identitylib.identifiers import IdentifierSchemes

from . import source, camsis
from .settings import load_settings
from .upload import google_storage_upload

LOG = logging.getLogger(os.path.basename(sys.argv[0]))


def main():
    # In principle __doc__ could be None so use f-strings to ensure input to docopt is always a
    # string.
    opts = docopt.docopt(f"{__doc__}")

    # Configure logging
    logging.basicConfig(
        level=logging.DEBUG if opts["--debug"] else
        logging.WARN if opts["--quiet"] else logging.INFO
    )

    # Read configuration files/objects/secrets in to settings. Pydantic takes care of validating
    # the format.
    settings = load_settings(opts["--configuration"])

    # Connect to Jackdaw.
    jd_connection = source.get_connection(**(settings.jackdaw.dict()))

    # Generate output as a JSON document.
    result = json.dumps(
        {"institutions": collect_institutions(jd_connection)},
        indent=settings.output.indent
    )

    # Write result to output.
    if settings.output.url.scheme == "stdout":
        LOG.info("Writing output to standard output...")
        sys.stdout.write(result)
    elif settings.output.url.scheme == "file":
        LOG.info("Writing output to \"%s\"...", settings.output.url.path)
        with open(settings.output.url.path, "w") as fobj:
            fobj.write(result)
    elif settings.output.url.scheme == "gs":
        LOG.info("Writing output to Google Storage at \"%s\"...", settings.output.url)
        google_storage_upload(
            settings.output.url.host,
            settings.output.url.path.lstrip("/"),
            result,
            timeout=settings.output.timeout
        )
    else:  # pragma: no cover
        # Should not be reached since scheme is verified in the settings module.
        raise NotImplementedError()


def collect_institutions(connection):
    """
    Collect CHRIS and CAMSIS institutions together into a single list of the form:

        [
            {
                "instid": <Lookup inst id>,
                "identifiers": [
                    "abc123@scheme1",
                    "xyz987@scheme2",
                    # ...
                ],
            },
            # ...
        ]

    """
    # Fetch CHRIS data into a dictionary keyed by Lookup instid.
    LOG.info("Fetching CHRIS institutions...")
    chris_data = {}
    for row in source.fetch(connection, "SELECT unitref, inst FROM chris_data.unit"):
        if row["inst"] is None:
            continue
        chris_data[row["inst"]] = chris_data.get(row["inst"], []) + [row["unitref"]]

    # Pre-seed CAMSIS data from static table.
    camsis_data = {inst: [dept] for dept, _, inst in camsis.STATIC_COLLEGES}

    # Fetch CAMSIS data into dictionary keyed by Lookup instid.
    # Join dept_inst and dept tables to filter to only real CAMSIS departments
    LOG.info("Fetching CAMSIS institutions...")
    for row in source.fetch(connection,
                            "SELECT d.dept, di.inst FROM camsis_data.dept_inst di, "
                            "camsis_data.dept d WHERE d.dept = di.dept"):
        if row["inst"] is None:
            continue
        camsis_data[row["inst"]] = camsis_data.get(row["inst"], []) + [row["dept"]]

    # Get list of all Lookup instids covered in ascending alphabetical order.
    instids = sorted(set(chris_data.keys()) | set(camsis_data.keys()))

    # Compute final result by merging data from CAMSIS and CHRIS.
    institutions = []
    for instid in instids:
        identifiers = [f'{instid}@{IdentifierSchemes.LOOKUP_INSTITUTION}']
        for chris_id in sorted(chris_data.get(instid, [])):
            identifiers.append(
                f'{chris_id}@{IdentifierSchemes.HR_INSTITUTION}')
        for camsis_id in sorted(camsis_data.get(instid, [])):
            identifiers.append(
                f'{camsis_id}@{IdentifierSchemes.STUDENT_INSTITUTION}')
        institutions.append({"instid": instid, "identifiers": identifiers})

    return institutions
