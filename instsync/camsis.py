# Static mapping providing:
#
#   (CamSIS college code, CamSIS description, Lookup instid)
#
# Generated manually from: https://www.camsis.cam.ac.uk/files/student-codes/a01.html
STATIC_COLLEGES = [
    ("CAI", "Gonville and Caius College", "CAIUS"),
    ("CC", "Corpus Christi College", "CORPUS"),
    ("CHR", "Christ's College", "CHRISTS"),
    ("CHU", "Churchill College", "CHURCH"),
    ("CL", "Clare College", "CLARE"),
    ("CLH", "Clare Hall", "CLAREH"),
    ("CTH", "St Catharine's College", "CATH"),
    ("DAR", "Darwin College", "DARWIN"),
    ("DOW", "Downing College", "DOWN"),
    ("ED", "St Edmund's College", "EDMUND"),
    ("EM", "Emmanuel College", "EMM"),
    ("F", "Fitzwilliam College", "FITZ"),
    ("G", "Girton College", "GIRTON"),
    ("HH", "Hughes Hall", "HUGHES"),
    ("HO", "Homerton College", "HOM"),
    ("JE", "Jesus College", "JESUS"),
    ("JN", "St John's College", "JOHNS"),
    ("K", "King's College", "KINGS"),
    ("LC", "Lucy Cavendish College", "LCC"),
    ("M", "Magdalene College", "MAGD"),
    ("N", "Newnham College", "NEWN"),
    ("NH", "Murray Edwards College", "NEWH"),
    ("PEM", "Pembroke College", "PEMB"),
    ("PET", "Peterhouse", "PET"),
    ("Q", "Queens' College", "QUEENS"),
    ("R", "Robinson College", "ROBIN"),
    ("SE", "Selwyn College", "SEL"),
    ("SID", "Sidney Sussex College", "SID"),
    ("T", "Trinity College", "TRIN"),
    ("TH", "Trinity Hall", "TRINH"),
    ("W", "Wolfson College", "WOLFC"),
]
