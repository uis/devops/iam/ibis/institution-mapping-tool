import logging
import re
from typing import Tuple, Optional, Sequence

import deepmerge
from pydantic import BaseModel, BaseSettings, validator, stricturl
from pydantic.env_settings import SettingsSourceCallable
import yaml

LOG = logging.getLogger(__name__)


def load_settings(paths: Sequence[str]) -> "Settings":
    """
    Load settings from a list of filesystem paths which point to YAML documents.

    """
    settings = {}
    for path in paths:
        LOG.info("Loading settings from %s", path)
        with open(path) as fobj:
            settings = deepmerge.always_merger.merge(settings, yaml.safe_load(fobj))
    return Settings.parse_obj(settings)


class Jackdaw(BaseModel):
    dsn: str
    username: str
    password: str

    @validator("dsn")
    def dsn_must_match_expected_format(cls, v):
        if not re.match("^[^:]*:[^/]*/.*$", v):
            raise ValueError("Jackdaw DSN must be of the form host:port/servicename")
        return v


OUTPUT_URL_SCHEMES = ["stdout", "file", "gs"]
OutputURL = stricturl(host_required=False, tld_required=False, allowed_schemes=OUTPUT_URL_SCHEMES)


class Output(BaseModel):
    url: OutputURL

    indent: Optional[int] = None

    timeout: int = 120

    @validator('url')
    def stdout_url_no_host_or_path(cls, v):
        if v.scheme == 'stdout' and (v.host is not None or v.path is not None):
            raise ValueError("stdout output URL must have no host or path")
        return v


class Settings(BaseSettings):
    jackdaw: Jackdaw
    output: Output

    class Config:
        env_prefix = 'instsync_'
        env_nested_delimiter = '__'

        @classmethod
        def customise_sources(
            cls,
            init_settings: SettingsSourceCallable,
            env_settings: SettingsSourceCallable,
            file_secret_settings: SettingsSourceCallable,
        ) -> Tuple[SettingsSourceCallable, ...]:
            return env_settings, file_secret_settings, init_settings
