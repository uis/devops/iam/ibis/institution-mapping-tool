from logging import getLogger

import cx_Oracle

LOG = getLogger(__name__)


def fetch(connection: cx_Oracle.Connection, statement: str, **kwargs):
    """
    Utility method to run a statement using the given connection and return a generator of dicts.

    """
    cursor = connection.cursor()
    cursor.execute(statement, **kwargs)

    field_names = [d[0].lower() for d in cursor.description]
    for row in cursor:
        yield {
            field_name: row[index]
            for index, field_name in enumerate(field_names)
        }


def get_connection(dsn: str, username: str, password: str):
    """
    Returns an oracle db connection using the given settings

    """

    LOG.info(f"Connecting to '{dsn}'")
    return cx_Oracle.connect(user=username, password=password, dsn=dsn)
