
from typing import Tuple, List


class MockCursorOf:
    """
    A cursor which mocks an Oracle cursor allowing results to be emitted
    from our `mock_results` input

    """

    def __init__(self, mock_results: List[List[Tuple]]):
        self.queries = list()
        self.mock_results = mock_results
        self.description = None
        self.next_result_iterator = None
        self.rowcount = 0

    def execute(self, query: str, **kwargs):
        """
        This is the function that gets called by application code to make a query,
        we then grab one of our results from mock_results and allow application
        code to start iterating on it

        """

        self.next_result_iterator = self.mock_results.pop(0) if self.mock_results else []
        # the first result should be a tuple containing the field names which we set as
        # our 'description' - the description is in an odd format, field names have to
        # be the first element in a tuple
        number_of_results = len(self.next_result_iterator)
        if number_of_results != 0:
            # we set the row to indicate the number of value rows that we have - excluding
            # the first row which is the header
            self.rowcount = number_of_results - 1
            self.description = [(field_name,) for field_name in self.next_result_iterator.pop(0)]
        else:
            self.rowcount = 0
            self.description = []

        self.queries.append((query, kwargs))

    def clear_current_execution(self):
        self.description = None
        self.next_result_iterator = None

    def __iter__(self):
        return self

    def __next__(self):
        if self.next_result_iterator is None:
            raise ValueError('No iterator to return from mock cursor')

        if len(self.next_result_iterator) == 0:
            self.next_result_iterator = None
            raise StopIteration

        return self.next_result_iterator.pop(0)


class MockConnection:
    """
    A mock to imitate an oracle db connection, returning a mocked cursor which
    will emit results using the `mock_results` passed in on our constructor.

    The mock results should be in the format of:
        Result for query -> Row -> Tuple of each field
    with the first tuple row containing the name of the fields which will be
    placed on the cursor's description. For example two independent query results
    each with a single row containing two fields would look like:
    [
        [
            ("firstname", "lastname")
            ("monty", "dawson")
        ],
        [
            ("id", "number")
            (100, 1)
        ]
    ]

    """

    base_url = 'https://test.com/legacydb'

    def __init__(self, mock_results: List[List[Tuple]] = []):
        self.mock_cursor = MockCursorOf(mock_results)

    def cursor(self):
        self.mock_cursor.clear_current_execution()
        return self.mock_cursor

    def get_queries_executed(self):
        return self.mock_cursor.queries
