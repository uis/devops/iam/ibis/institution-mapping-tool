import os
import json
import tempfile
from unittest import TestCase, mock

import yaml
from identitylib.identifiers import IdentifierSchemes

from .. import main

from .db_mock import MockConnection

# Mock data from CHRIS.
CHRIS_MOCK_DATA = [
    {'inst': 'ABC', 'unitref': 'U0001'},
    {'inst': 'ABC', 'unitref': 'U0002'},
    {'inst': 'DEF', 'unitref': 'U0003'},
    {'inst': None, 'unitref': 'U0004'},
]

# Mock data from CAMSIS
CAMSIS_MOCK_DATA = [
    {'inst': 'ABC', 'dept': 'A'},
    {'inst': 'GHI', 'dept': 'G'},
    {'inst': None, 'dept': 'X'},
]

# Mock static college data from CAMSIS
CAMSIS_MOCK_COLLEGE_DATA = [
    ('CAI', 'Gonville and Caius College', 'CAIUS'),
    ('CC', 'Corpus Christi College', 'CORPUS'),
    ('CHR', 'Christ\'s College', 'CHRISTS'),
]

# Expected output from above
EXPECTED_OUTPUT = {
    'institutions': [
        {
            'instid': 'ABC',
            'identifiers': [
                f'ABC@{IdentifierSchemes.LOOKUP_INSTITUTION}',
                f'U0001@{IdentifierSchemes.HR_INSTITUTION}',
                f'U0002@{IdentifierSchemes.HR_INSTITUTION}',
                f'A@{IdentifierSchemes.STUDENT_INSTITUTION}',
            ],
        }, {
            'instid': 'CAIUS',
            'identifiers': [
                f'CAIUS@{IdentifierSchemes.LOOKUP_INSTITUTION}',
                f'CAI@{IdentifierSchemes.STUDENT_INSTITUTION}',
            ],
        }, {
            'instid': 'CHRISTS',
            'identifiers': [
                f'CHRISTS@{IdentifierSchemes.LOOKUP_INSTITUTION}',
                f'CHR@{IdentifierSchemes.STUDENT_INSTITUTION}',
            ],
        }, {
            'instid': 'CORPUS',
            'identifiers': [
                f'CORPUS@{IdentifierSchemes.LOOKUP_INSTITUTION}',
                f'CC@{IdentifierSchemes.STUDENT_INSTITUTION}',
            ],
        },
        {
            'instid': 'DEF',
            'identifiers': [
                f'DEF@{IdentifierSchemes.LOOKUP_INSTITUTION}',
                f'U0003@{IdentifierSchemes.HR_INSTITUTION}',
            ],
        }, {
            'instid': 'GHI',
            'identifiers': [
                f'GHI@{IdentifierSchemes.LOOKUP_INSTITUTION}',
                f'G@{IdentifierSchemes.STUDENT_INSTITUTION}'
            ]
        }
    ]
}


class InstitutionsTestCase(TestCase):
    def setUp(self):
        patcher = mock.patch('os.environ', new_callable=dict)
        self.environ_mock = patcher.start()
        self.addCleanup(patcher.stop)

        # Known good settings.
        settings_dict = {
            'jackdaw': {'dsn': 'host:port/service', 'username': 'user', 'password': 'pass'},
            'output': {'url': 'stdout://'}
        }

        # Write settings to configuration file.
        tmpdir = tempfile.TemporaryDirectory()
        self.addCleanup(tmpdir.cleanup)
        config_file = os.path.join(tmpdir.name, 'config.yaml')
        with open(config_file, 'w') as fobj:
            yaml.dump(settings_dict, fobj)

        self.opts_mock = {'--quiet': False, '--debug': False, '--configuration': [config_file]}
        patcher = mock.patch('docopt.docopt')
        docopt_mock = patcher.start()
        self.addCleanup(patcher.stop)
        docopt_mock.return_value = self.opts_mock

        patcher = mock.patch('instsync.camsis.STATIC_COLLEGES', CAMSIS_MOCK_COLLEGE_DATA)
        patcher.start()
        self.addCleanup(patcher.stop)

        self.mock_connection = MockConnection()
        patcher = mock.patch('instsync.source.get_connection')
        get_connection_mock = patcher.start()
        get_connection_mock.return_value = self.mock_connection
        self.addCleanup(patcher.stop)

        patcher = mock.patch('instsync.source.fetch', self._mock_source_fetch)
        patcher.start()
        self.addCleanup(patcher.stop)

    def _mock_source_fetch(self, connection, query):
        """
        Mock version of source.fetch() which returns our mock results for appropriate queries.

        """
        self.assertIs(connection, self.mock_connection)
        if query == 'SELECT unitref, inst FROM chris_data.unit':
            return CHRIS_MOCK_DATA
        elif query == ('SELECT d.dept, di.inst FROM camsis_data.dept_inst di, camsis_data.dept d '
                       'WHERE d.dept = di.dept'):
            return CAMSIS_MOCK_DATA
        raise RuntimeError(f'Unexpected query: {query!r}')

    def test_basic_functionality(self):
        with mock.patch('sys.stdout.write') as write_mock:
            main()
        write_mock.assert_called()

    def test_file_url(self):
        '''
        Passing a file URL writes output to that file.

        '''
        with tempfile.TemporaryDirectory() as tmpdir:
            output_file = os.path.join(tmpdir, 'output.json')
            self.environ_mock['INSTSYNC_OUTPUT__URL'] = f'file://{output_file}'
            main()
            self.assertTrue(os.path.isfile(output_file))

    def test_gs_url(self):
        '''
        Passing a gs URL writes output to bucket.

        '''
        self.environ_mock['INSTSYNC_OUTPUT__URL'] = 'gs://my-bucket/path/to/object.json'
        with mock.patch('instsync.google_storage_upload') as mock_upload:
            main()
        mock_upload.assert_called()
        args, _ = mock_upload.call_args
        self.assertEqual(args[0], 'my-bucket')
        self.assertEqual(args[1], 'path/to/object.json')

    def test_functionality(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            output_file = os.path.join(tmpdir, 'output.json')
            self.environ_mock['INSTSYNC_OUTPUT__URL'] = f'file://{output_file}'
            main()
            with open(output_file) as fobj:
                output = json.load(fobj)
        self.assertEqual(output, EXPECTED_OUTPUT)
