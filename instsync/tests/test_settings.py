import os
import tempfile
from unittest import TestCase, mock

import yaml

from ..settings import Settings, load_settings


class SettingsTestCase(TestCase):
    def setUp(self):
        patcher = mock.patch("os.environ", new_callable=dict)
        self.environ_mock = patcher.start()
        self.addCleanup(patcher.stop)

        # Known good settings.
        self.settings_dict = {
            "jackdaw": {"dsn": "host:port/service", "username": "user", "password": "pass"},
            "output": {"url": "stdout://"}
        }

    def test_basic_settings(self):
        """
        Basic setting loading should succeed without throwing.

        """
        s = Settings.parse_obj(self.settings_dict)
        self.assertEqual(s.output.url.scheme, "stdout")

    def test_bad_dsn(self):
        """
        The Jackdaw DSN should be of the form "host:port/service".

        """
        self.settings_dict["jackdaw"]["dsn"] = "bad-dsn"
        with self.assertRaises(ValueError):
            Settings.parse_obj(self.settings_dict)

    def test_bad_output_scheme(self):
        """
        The output URL should have a supported scheme.

        """
        self.settings_dict["output"]["url"] = "https://example.com/"
        with self.assertRaises(ValueError):
            Settings.parse_obj(self.settings_dict)

    def test_bad_stdout_output(self):
        """
        The stdout URL should not have a host or a path

        """
        self.settings_dict["output"]["url"] = "stdout://host"
        with self.assertRaises(ValueError):
            Settings.parse_obj(self.settings_dict)
        self.settings_dict["output"]["url"] = "stdout:///path"
        with self.assertRaises(ValueError):
            Settings.parse_obj(self.settings_dict)

    def test_file_output(self):
        """
        The output URL supports the file scheme.

        """
        self.settings_dict["output"]["url"] = "file:///tmp/foo"
        s = Settings.parse_obj(self.settings_dict)
        self.assertEqual(s.output.url.scheme, "file")
        self.assertEqual(s.output.url.path, "/tmp/foo")

    def test_gs_output(self):
        """
        The output URL supports the Google Storage scheme.

        """
        self.settings_dict["output"]["url"] = "gs://bucket/path/to/object"
        s = Settings.parse_obj(self.settings_dict)
        self.assertEqual(s.output.url.scheme, "gs")
        self.assertEqual(s.output.url.host, "bucket")
        self.assertEqual(s.output.url.path, "/path/to/object")

    def test_loading_settings(self):
        """
        Settings from multiple files are merged.

        """
        with tempfile.TemporaryDirectory() as tmpdir:
            base = os.path.join(tmpdir, "base.yaml")
            with open(base, "w") as fobj:
                yaml.dump(self.settings_dict, fobj)
            extra = os.path.join(tmpdir, "extra.yaml")
            with open(extra, "w") as fobj:
                fobj.write('{"output": {"url": "file:///tmp/foo"}}')
            s = load_settings([base, extra])
        self.assertEqual(s.jackdaw.username, self.settings_dict["jackdaw"]["username"])
        self.assertEqual(s.output.url, "file:///tmp/foo")
