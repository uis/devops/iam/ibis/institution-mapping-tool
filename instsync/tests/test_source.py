from unittest import TestCase, mock

from .. import source
from .db_mock import MockConnection


class SourceTestCase(TestCase):
    def setUp(self):
        patcher = mock.patch("cx_Oracle.connect")
        self.connect_mock = patcher.start()
        self.addCleanup(patcher.stop)

    def test_basic_mock_functionality(self):
        mock_connection = MockConnection([
            [("foo", "bar"), (1, 2)]
        ])
        rows = list(source.fetch(mock_connection, "MOCK SELECT"))
        self.assertEqual(rows, [{"foo": 1, "bar": 2}])

    def test_get_connection(self):
        c = source.get_connection("dsn", "user", "pass")
        self.connect_mock.assert_called_with(user="user", password="pass", dsn="dsn")
        self.assertIs(c, self.connect_mock.return_value)
