from unittest import TestCase, mock

from .. import upload


class GoogleStorageTestCase(TestCase):
    def setUp(self):
        patcher = mock.patch("google.cloud.storage.Client")
        client_class_mock = patcher.start()
        self.addCleanup(patcher.stop)
        self.client_mock = client_class_mock.return_value

    def test_basic_upload(self):
        upload.google_storage_upload("bucket", "path/to/object", "content", timeout=12)
        self.client_mock.bucket.assert_called_with("bucket")
        self.client_mock.bucket.return_value.blob.assert_called_with("path/to/object")
        blob = self.client_mock.bucket.return_value.blob.return_value
        blob.upload_from_string.assert_called_with(
            "content", content_type="application/json", timeout=12)
