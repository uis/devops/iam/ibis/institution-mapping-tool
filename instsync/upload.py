import logging
from google.cloud import storage

LOG = logging.getLogger(__name__)


def google_storage_upload(
        bucket_name, object_name, content, timeout=120, content_type="application/json"):
    """
    Upload content to a Google Storage object.

    """
    LOG.info(f"Connecting to bucket '{bucket_name}'")
    client = storage.Client()
    bucket = client.bucket(bucket_name)
    LOG.info(f"Creating object '{object_name}'")
    blob = bucket.blob(object_name)
    blob.upload_from_string(content, content_type=content_type, timeout=timeout)
