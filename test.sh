#
# Wrapper script to run tox. Arguments are passed directly to tox.

# Exit on failure
set -e

# Execute tox runner, logging command used
set -x
exec docker-compose --file ./test-compose.yml run --rm tox tox $@